package com.aqa.StepDefinition;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.Reporter;

import com.aqa.base.BrowserSetup;

public class SortDesc extends BrowserSetup {
	@SuppressWarnings("deprecation")
	public void Descending()throws InterruptedException {
		//Change the sorting to Name ( Z -> A). 
		driver.findElement(By.xpath(locators.getProperty("sort"))).click();
		
		//before filter capture the prices
		List<WebElement> beforeFilterPrice = driver.findElements(By.className("inventory_item_price"));
		
		//remove the $symbol from the price and convert the string into double
		List<Double>beforeFilterPriceList = new ArrayList<Double>();
		
		for(WebElement p : beforeFilterPrice) {
			beforeFilterPriceList.add(Double.valueOf(p.getText().replace( "$", "")));
			
		}
		
		// filter the price from the drop down
		Select dropdown = new Select(driver.findElement(By.className("product_sort_container")));
		dropdown.selectByVisibleText("Price (high to low)");
		
		//After filter capture the prices
		List<WebElement> afterFilterPrice = driver.findElements(By.className("inventory_item_price"));
		
		// remove $ symbol from the price and convert the string into double
		List<Double> afterFilterPriceList = new ArrayList<Double>();
		
		for(WebElement p : afterFilterPrice) {
			afterFilterPriceList.add(Double.valueOf(p.getText().replace(  "$", "")));
		}
		
		//Compare the values/Assert the values(first we need to sort the values of before filterPrice)
		Collections.sort(beforeFilterPriceList);// it will sort the values in the list
		Collections.reverse(beforeFilterPriceList); //List will get sorted in the form of descending order
		Assert.assertEquals(beforeFilterPriceList, afterFilterPriceList);
		
		Thread.sleep(3000);
		driver.quit();
		Reporter.log("Application closed");
	}
	@SuppressWarnings("deprecation")
	public void ValidLogin2()throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().window().setSize(new Dimension(1296, 688));
		//Click on the username field and enter username
		//driver.findElement(By.className(locators.getProperty("UsernameeField"))).click();
		driver.findElement(By.xpath(locators.getProperty("UserNameeField")))
		.sendKeys(prop.getProperty("QTUsername"));
		
		driver.findElement(By.xpath(locators.getProperty("PasswordField")))
		.sendKeys(prop.getProperty("QTPassword"));
		
		//Click on the Login
	
		driver.findElement(By.cssSelector(locators.getProperty("LoginBTN"))).click();
		//driver.manage().wait(10);	
		
	}
	

}
