package com.aqa.StepDefinition;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.Reporter;

import com.aqa.base.BrowserSetup;

public class Login extends BrowserSetup {
	
	@SuppressWarnings("deprecation")
	public void ValidLogin()throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().window().setSize(new Dimension(1296, 688));
		//Click on the username field and enter username
		//driver.findElement(By.className(locators.getProperty("UsernameeField"))).click();
		driver.findElement(By.xpath(locators.getProperty("UserNameeField")))
		.sendKeys(prop.getProperty("QTUsername"));
		
		driver.findElement(By.xpath(locators.getProperty("PasswordField")))
		.sendKeys(prop.getProperty("QTPassword"));
		
		//Click on the Login
	
		driver.findElement(By.cssSelector(locators.getProperty("LoginBTN"))).click();
		//driver.manage().wait(10);	
		Reporter.log("Application closed");
		
	}
	
	
	
}
	

