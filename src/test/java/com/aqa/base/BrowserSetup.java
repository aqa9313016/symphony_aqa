package com.aqa.base;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import io.github.bonigarcia.wdm.WebDriverManager;

public class BrowserSetup {
	public static WebDriver driver;
	public static String browser = "chrome";

	public static FileReader fr;
	public static Properties prop = new Properties();
	public static FileReader loc;
	public static Properties locators = new Properties();
	
	@BeforeMethod
	// @Parameters({"browser"})
	public void launch_browser() throws InterruptedException, IOException {
		// To read the URl from the config.properties file
		FileReader fr = new FileReader(
				System.getProperty("user.dir") + "\\src\\test\\resources\\ConfigFiles\\BrowserSetupp\\BrowserSetup.properties");
		FileReader loc = new FileReader(
				System.getProperty("user.dir") + "\\src\\test\\resources\\Locators\\Locators.properties");
		prop.load(fr);
		locators.load(loc);

		if (browser.equals("firefox")) {
			WebDriverManager.firefoxdriver().setup();
			driver = new FirefoxDriver();
			driver.get(prop.getProperty("Url"));
			driver.manage().window().maximize();
			driver.findElement(By.xpath(locators.getProperty("popup_xpath"))).click();
		} else if (browser.equals("chrome")) {
			ChromeOptions option = new ChromeOptions();
			option.addArguments("--remote-allow-origins=*","ignore-certificate-errors");
			WebDriverManager.chromedriver().setup();
			driver = new ChromeDriver(option);
			driver.get(prop.getProperty("Url"));
			driver.manage().window().maximize();
		//	driver.findElement(By.className(locators.getProperty("UsernameeField"))).click();
		} else if (browser.equals("edge")) {
			WebDriverManager.edgedriver().setup();
			driver = new EdgeDriver();
			driver.get(prop.getProperty("Url"));
			driver.manage().window().maximize();
			//driver.findElement(By.xpath(locators.getProperty("popup_xpath"))).click();
		}
	}
	
	@AfterMethod
	public void close_browser() {
		driver.quit();
	}


}
