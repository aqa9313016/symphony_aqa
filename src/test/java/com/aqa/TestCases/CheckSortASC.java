package com.aqa.TestCases;

import org.testng.annotations.Test;

import com.aqa.StepDefinition.SortAsc;
import com.aqa.StepDefinition.Login;

public class CheckSortASC extends SortAsc {
	@Test(priority=1,description= "Verify that items are sorted A - Z in ascending order")
	public void CheckSort() throws InterruptedException {
		ValidLogin1();
		 Ascending();
		
	}

}
