# Selenium-Java-Allure Framework

A Maven framework in which to build Selenium tests written in Java with Allure reports of test results.

## Getting Started

Copy the repo into your local machine.

### Run tests locally

Right click the feature file and select "Run" or "Debug" to start the test.

### Run tests through the commandline

As this project uses Maven, we can invoke the tests using Maven goals.

To run the test, use your CI or point Maven to the project and use the goals:

```
clean install site
```

```
$ mvn -Dtest=LoginScenarios test
```
### Run multiple tests through the commandline
```
$ mvn -Dtest=LoginScenarios,CheckSortDESC, CheckSortASC test
```
```
clean install site
```

### Defining the browser

By default, the project will default to ChromeLocal (running a local Chrome instance) if no browser is specified.

To express a specific browser type, at runtime or through VM options in your IDE, pass the following property:

```
mvn test -Dbrowser=firefox

```

##### Common Parameter Values

| Browser type                    | System property value      |
|:--------------------------------|:---------------------------|
|Chrome (Local)                   | ChromeLocal                |
|Chrome (Remote)                  | ChromeRemote               |
|Firefox (Local)                  | FirefoxLocal               |
|Firefox (Remote)                 | FirefoxRemote              |
|Opera (Local)                    | OperaLocal                 |
|Opera (Remote)                   | OperaRemote                |
|Safari (Local)                   | SafariLocal                |
|Safari (Remote)                  | SafariRemote               |
|Edge (Local)                     | EdgeLocal                  |
|Edge (Remote)                    | EdgeRemote                 |
|Edge (Local)                     | EdgeLocal                  |
|Edge (Remote)                    | EdgeRemote                 |
|Internet Explorer (Local)        | InternetExplorerLocal      |
|Internet Explorer (Remote)       | InternetExplorerRemote     |
|Chrome Mobile Emulation (Local)  | ChromeMobileEmulationLocal |
|Chrome Mobile Emulation (Remote) | ChromeMobileEmulationRemote|

```

## Writing tests

To write tests, you can call any Webdriver methods by calling:
```
DriverManager.getDriver()
```

This will allow you access all the available methods to all Webdrivers as outlined by the W3C standard.

##### Base class
This is a "helper" class that helps instantiate the browser type and also calls the configuration and locator property files.

All classes within the step definition package define all default test scenarios within the scope of this project it has methods such as Valid login, check sort in ascending order, check sort in descending order.

##### Testcases* Package

Additionally, All testcase classes are defined under the TestCases package, to run the tests on eclipse you can either run each by using test ng or running the testng.xml file to run all testcases 

##### Properties

Properties are retrieved by default from a file called **ConfigFiles**,**Locators**, which should be located under:

src > main > resources > 

The properties declared here can be overriden, however, if specified as a system property at runtime.

For example, if you declare the following property in your **com.aqa..base >BrowserSetup** file:

```
browser = "chrome";
```

And you also pass the following at runtime:

```
-Dbrowser.type=EdgeLocal
```

The property browser.type will be set to EdgeLocal, as the system property overrides the properties file's values.


## Reporting

The default reporting provided by the framework is Allure.
By default, screenshots are taken after each step and after a failure (if any), which will display on each step of the report.
To turn this behaviour off and disable screenshots, pass the following property:

```
-Dscreenshot.off=TRUE
```

To take additional screenshots, you can call the following method at any point, and it will automatically add a screenshot to the relevant step:

```
ScreenshotTaker.attachScreenshot();
```

N.B. Although Allure is the default reporter, the TestNG/JUnit reports generated can be used with any compatible reporter in your project if you choose not to use Allure.


## Built With

* [Selenium](https://github.com/SeleniumHQ/selenium) - Browser automation framework
* [Maven](https://maven.apache.org/) - Dependency management
* [TestNG](https://github.com/cbeust/testng) - Testing framework
* [Allure](https://github.com/allure-framework) - Reporting framework
* [WebDriverManager](https://github.com/bonigarcia/webdrivermanager) - Local driver binary management

